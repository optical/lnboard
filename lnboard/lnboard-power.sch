EESchema Schematic File Version 4
LIBS:lnboard-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L EA3036:EA3036 U?
U 1 1 5D33226F
P 6950 1650
F 0 "U?" H 6925 2565 50  0000 C CNN
F 1 "EA3036" H 6925 2474 50  0000 C CNN
F 2 "" H 6950 1650 50  0001 C CNN
F 3 "" H 6950 1650 50  0001 C CNN
	1    6950 1650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D333889
P 7000 3000
F 0 "#PWR?" H 7000 2750 50  0001 C CNN
F 1 "GND" H 7005 2827 50  0000 C CNN
F 2 "" H 7000 3000 50  0001 C CNN
F 3 "" H 7000 3000 50  0001 C CNN
	1    7000 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3000 7000 2900
Wire Wire Line
	7000 2900 6900 2900
Wire Wire Line
	6700 2900 6700 2850
Wire Wire Line
	6800 2850 6800 2900
Connection ~ 6800 2900
Wire Wire Line
	6800 2900 6700 2900
Wire Wire Line
	6900 2850 6900 2900
Connection ~ 6900 2900
Wire Wire Line
	6900 2900 6800 2900
Wire Wire Line
	7000 2850 7000 2900
Connection ~ 7000 2900
Wire Wire Line
	7100 2900 7100 2850
Wire Wire Line
	7100 2900 7000 2900
Wire Wire Line
	7100 2900 7200 2900
Wire Wire Line
	7200 2900 7200 2850
Connection ~ 7100 2900
Wire Wire Line
	7200 2900 7300 2900
Wire Wire Line
	7300 2900 7300 2850
Connection ~ 7200 2900
$EndSCHEMATC
